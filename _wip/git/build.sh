#!/bin/bash

make configure
chmod +x configure

LDFLAGS="-L$PREFIX/lib" CFLAGS="-I$PREFIX/include" ./configure \
--prefix=$PREFIX \
--with-libpcre \
--with-gitconfig=$PREFIX/etc/gitconfig \
--with-gitattributes=$PREFIX/etc/gitattributes

LDFLAGS="-L$PREFIX/lib" CFLAGS="-I$PREFIX/include" make -j `nproc`
LDFLAGS="-L$PREFIX/lib" CFLAGS="-I$PREFIX/include" make install
