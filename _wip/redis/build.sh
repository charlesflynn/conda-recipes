#!/bin/bash

make distclean
#cd $SRC_DIR/deps/hiredis
#make static
#cd $SRC_DIR/deps
#make lua jemalloc linenoise
#cd $SRC_DIR/src
make -j `nproc`
make PREFIX=$PREFIX/opt/redis install
