#!/bin/bash

chmod +x configure

./configure --prefix=$PREFIX/opt/postgresql \
    --with-includes=$PREFIX/include --with-libraries=$PREFIX/lib \
    --with-python --with-openssl \
    --with-libxml --with-libxslt 

gmake -j `nproc`
make install-strip
