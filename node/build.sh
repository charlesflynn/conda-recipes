#!/bin/bash

mkdir -p $PREFIX/opt/node
cp -rp  $SRC_DIR/* $PREFIX/opt/node
chmod -R +x $PREFIX/opt/node/bin
cd $PREFIX/bin
ln -s ../opt/node/bin/node
ln -s ../opt/node/bin/npm
